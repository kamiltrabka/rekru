from django.shortcuts import render
from django.http import HttpResponse

from urllib.request import urlopen
from bs4 import BeautifulSoup
import urllib.error
import re


def index(request):
	web_address = ''
	file_content = ''
	key_words = []
	words_list = []
	err = ''
	if request.GET.get('form_address'):
		web_address = request.GET.get('form_address')
		try:
			file_content = urlopen(web_address).read().decode('utf-8')
		except urllib.error.URLError as e:
			err = 'URLError'
		except urllib.error.HTTPError as e:
			err = 'HTTPError'
		except ValueError as e:
			err = 'ValueError'
		soup = BeautifulSoup(file_content)
		metas = soup.find_all('meta')		
		regex = re.compile(r'keywords', re.I)
		for meta in metas:
			if 'name' in meta.attrs and re.match(regex, meta.attrs['name']):
				key_words_str = meta.attrs['content']
				key_words = key_words_str.split(",")
				web_text = BeautifulSoup(file_content, "lxml").text
				for words in key_words:
					count = sum(1 for _ in re.finditer(r'\b%s\b' % re.escape(words), web_text))
					words_list.append((words,count))
				words_list.sort(key=lambda x: x[1], reverse = True)				
	context = {'web_address': web_address, 'err': err, 'words_list': words_list}
	return render(request, 'app/index.html', context)

